// -*- tab-width: 4; js-indent-level: 4; -*-
// ==UserScript==
// @name         Trakt Storage Clearer
// @namespace    danielrayjones
// @version      0.0.1
// @description  
// @author       Dan Jones
// @match        https://trakt.tv/users/*/progress*
// @grant        none
// @require      https://bowercdn.net/c/konami-code-1.3.2/src/jquery.konami.min.js
// ==/UserScript==

/* global $, compressedCache, localStorage, MutationObserver */
/* jshint esversion: 6 */

(function() {
    'use strict';

    const clearStorage = () => localStorage.clear();

    /* todo
    $(window).konami({
        code: [80, 73, 67, 75],
        cheat: clearStorage
    });
    */

    function addPickButton() {
        const $leftNav = $('.subnav-wrapper .container .left');
        const $found = $leftNav.find('.clear-store');
        if (!$found.length) {
            $leftNav.append('<span class="filter-dropdown toggle-simple-progress clear-store" title="Clear Storage"><span class="icon trakt-icon-cancel"></span>X</span>')
                .find('.clear-store').on('click', clearStorage);
        }
    }

})();
