// ==UserScript==
// @name Old Reddit Image Expander
// @namespace danielrayjones
// @description Allow removal of visited links on Amazon Giveaway Listing
// @include https://old.reddit.com*
// @version 1.0.3
// ==/UserScript==

(function() {
    const listing = document.getElementById('siteTable');

    if (!listing) {
        return;
    }

    let para = document.createElement('p');
    let exButton = document.createElement('button');
    exButton.innerText = 'Expand';
    exButton = para.appendChild(exButton);
    exButton.addEventListener('click', () => document.querySelectorAll('.expando-button.collapsed').forEach(e => e.click()));

    let deButton = document.createElement('button');
    deButton.innerText = 'Collapse';
    deButton = para.appendChild(deButton);
    deButton.addEventListener('click', () => document.querySelectorAll('.expando-button.expanded').forEach(e => e.click()));

    para = listing.insertBefore(para, listing.firstChild);

    const proxyThumbClicks = function(evt) {
        let thing = evt.target;
        while (!thing.classList.contains('thing') && thing != document.body) {
            thing = thing.parentElement;
        }
        if (!thing.classList.contains('thing')) {
            console.log("Couldn't find thing");
            return true;
        }
        evt.preventDefault();

        thing.querySelector('.expando-button')?.click();

        return false;
    };

    listing.querySelectorAll('.thumbnail').forEach(e => e.addEventListener('click', proxyThumbClicks));
})();
